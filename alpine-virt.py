import argparse, pexpect, sys

parser = argparse.ArgumentParser(description='Quemu for Alpine virtual.')
parser.add_argument('command_line', metavar='C', nargs='?',
                    help='command line for qemu',
                    default='qemu-system-x86_64  -cdrom alpine-virt-3.7.0-x86_64.iso -nographic') # -enable-kvm # not yet but check for future! 
args = parser.parse_args()

child = pexpect.spawn(args.command_line)
# child.logfile = sys.stdout
child.logfile_read = sys.stdout
# child.expect('\rlocalhost login: ')
child.expect('localhost login: ', timeout=120)
child.sendline('root')
# child.expect('localhost:~#')
child.expect('localhost:')
child.sendline('setup-alpine -qe') # Does the network work without this?
child.expect('Select keyboard layout.*:')
child.sendline('')
child.expect('Which one do you want to initialize?') ###############
child.sendline('')
child.expect('Ip address for')
child.sendline('')
child.expect('Do you want to do any manual network configuration?')
child.sendline('')
# child.expect('alpine:~#')
child.expect('alpine:')
child.sendline('apk update && apk upgrade --no-progress && apk add curl --no-progress')
child.expect('alpine:')
# child.sendline('apk upgrade --no-progress')
# child.expect('alpine:')
# child.sendline('apk add curl --no-progress')
# child.expect('alpine:')
# child.sendline('apk add psmisc --no-progress') # community repository not installed (needed by psmisc)
# child.expect('alpine:')
# child.sendline('hash -r') # Re-read PATH environment variable (probably not needed)
# child.expect('alpine:')
child.sendline('uname -a')
child.expect('alpine:')
child.sendline('cat /proc/cmdline')
child.expect('alpine:')
child.sendline('ifconfig')
child.expect('alpine:')
child.sendline('ping -c 1 gitlab.com') # Qemu (in network "user" mode) blocks ping's kind of packets.
child.expect('alpine:')
child.sendline('curl gitlab.com --silent --show-error > /dev/null')
child.expect('alpine:')
# child.sendline('rc-status')
# child.expect('alpine:')
# child.sendline('command -v pstree')
# child.expect('alpine:')
# child.sendline('which -a pstree')
# child.expect('alpine:')
# child.sendline('pstree --unicode # from psmisc') # not installed
# child.expect('alpine:')
# child.sendline('pstree # from BusyBox')
# child.expect('alpine:')
# child.sendline('apk')
# child.expect('alpine:')
child.sendline('time sh -c "apk add bridge lxc --no-progress" > /dev/null') # Maybe lxc -> lxc-utils at some point
child.expect('alpine:')
child.sendline('rc-status')
child.expect('alpine:')
child.sendline('pstree # from BusyBox')
child.expect('alpine:')
child.sendline('ls /etc/lxc')
child.expect('alpine:')
child.sendline('cat /etc/lxc/default.conf')
child.expect('alpine:')
child.sendline('echo "lxc.net.0.type = none" >> /etc/lxc/simple.conf')
# unprivileged containers do not work with this setting
# https://linuxcontainers.org/fr/lxc/manpages/man5/lxc.container.conf.5.html
child.expect('alpine:')
# Basic privileged usage
# child.sendline('lxc-create --template download --name u1 -- --dist ubuntu --release bionic --arch amd64')
# Needs: xz-utils wget # Recommends: gpg
child.sendline('lxc-create --version') 
child.expect('alpine:')
# child.sendline('lxc-create --help') 
# child.expect('alpine:')
child.sendline('ls /etc/lxc/templates /usr/share/lxc/templates') 
child.expect('alpine:')
# child.sendline('lxc-create --template ubuntu --name u1') # 
child.sendline('lxc-create --template alpine --help') 
child.expect('alpine:')
child.sendline('lxc-create --template alpine --name simple --config=/etc/lxc/simple.conf -- --release=v3.7') # --debug curl nginx
child.expect('alpine:')
child.sendline('cat /var/lib/lxc/simple/config')
child.expect('alpine:')
child.sendline('lxc-ls --fancy')
child.expect('alpine:')
child.sendline('lxc-start --name simple --foreground') # --daemon --logfile=-
# * Starting networking ... *   eth0 (to check)
child.expect('login: ', timeout=120) # simple login:
child.sendline('root') # simple:~#

# child.expect('alpine:')
# child.sendline('lxc-ls --fancy')
child.expect('simple:')
if 0:
    child.sendline('setup-alpine -qe') # This is not needed to use the network.
    # child.expect('Select keyboard layout.*:')
    # child.sendline('')
    child.expect('Which one do you want to initialize?') ###############
    child.sendline('')
    child.expect('Ip address for')
    child.sendline('')
    child.expect('Netmask?')
    child.sendline('')
    child.expect('Gateway?')
    child.sendline('')
    child.expect('Do you want to do any manual network configuration?')
    child.sendline('')
    child.expect('DNS domain name?')
    child.sendline('')
    child.expect('DNS nameserver(s)?')
    child.sendline('')
    # hostname: sethostname: Operation not permitted
    child.expect('simple:')
# One should test needed setup is working! (seems OK)
child.sendline('whoami')
child.expect('simple:')
child.sendline('uname -a')
child.expect('simple:')
child.sendline('pstree')
child.expect('simple:')
child.sendline('rc-status')
child.expect('simple:')
# child.sendline('curl gitlab.com --silent --show-error > /dev/null')
# child.expect('simple:')
child.sendline('apk update')
child.expect('simple:')
child.sendline('halt')

child.expect('alpine:')
child.sendline('lxc-ls --fancy')
child.expect('alpine:')
if child.isalive():
    child.sendline('halt')
    child.expect('reboot: System halted')
    # print(child.before)
    child.close()
# Print the final state of the child. Normally isalive() should be FALSE.
print
if child.isalive():
    print('Child did not exit gracefully.')
else:
    print('Child exited gracefully.')
print(child.exitstatus, child.signalstatus)