* This would allow to test different CPU architctures with qemu emulation!

## TODO
* Update download links!!
* With LXC do
  * a simple guest with "lxc.net.0.type = none" ([Howto-lxc-simple](https://wiki.alpinelinux.org/wiki/Howto-lxc-simple)) Done!
    * Try it on Ubuntu host...
      [qemu-demo/ubuntu-cloud](https://gitlab.com/qemu-demo/ubuntu-cloud/)
  * a networked guest with "lxc.net.0.type = veth" ([LXC](https://wiki.alpinelinux.org/wiki/LXC))
* test modprob kvm-intel on more distributions than Alpine and try to understand
  * modprobe: can't change directory to '/lib/modules': No such file or directory
  * It is rare in 2018-09 to have a CPU with the extensions...
* Link and separate what is qemu and what is LXC, find information like man pages and --help in separate jobs in the right project.

## LXC (should go in a separate project, but it is a good application of qemu)
### Alpine host
* [Howto-lxc-simple](https://wiki.alpinelinux.org/wiki/Howto-lxc-simple)
* [LXC](https://wiki.alpinelinux.org/wiki/LXC)
* [Install Alpine on LXC](https://wiki.alpinelinux.org/wiki/Install_Alpine_on_LXC)

## Alpine Linux download and install
* https://alpinelinux.org/downloads/
  * http://dl-cdn.alpinelinux.org/alpine/
* https://wiki.alpinelinux.org/wiki/Installation
* [Alpine setup scripts](https://wiki.alpinelinux.org/wiki/Alpine_setup_scripts) 2016
  * Same thing manually [Configure Networking](https://wiki.alpinelinux.org/wiki/Configure_Networking) 2017

### "Setup" with alpine-conf
* https://github.com/alpinelinux/alpine-conf

## Finding a CD image for the serial console
* [linux live cd for serial console](https://google.com/search?q=linux+live+cd+for+serial+console)
* [live cd for serial console](https://google.com/search?q=live+cd+for+serial+console)
* https://en.wikipedia.org/wiki/Alpine_Linux
 
## Qemu
* CPU capability: [qemu KVM kernel module no such file or directory](https://stackoverflow.com/questions/14542754/qemu-kvm-kernel-module-no-such-file-or-directory)
* [Using the user mode network stack](https://qemu.weilnetz.de/doc/qemu-doc.html#Using-the-user-mode-network-stack) from [QEMU version 2.12.50 User Documentation](https://qemu.weilnetz.de/doc/qemu-doc.html)
* [QEMU documentation](https://www.qemu.org/documentation/)
* [Documentation/Networking](https://wiki.qemu.org/Documentation/Networking)
* [QEMU](https://wiki.archlinux.org/index.php/QEMU) @ [wiki.archlinux.org](https://wiki.archlinux.org/)


## Similar project(s)
* [travis-util/qemu](https://github.com/travis-util/qemu)@github
* [hub-docker-com-demo/qemu](https://gitlab.com/hub-docker-com-demo/qemu)@gitlab